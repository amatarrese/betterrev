package org.adoptopenjdk.betterrev.models;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.adoptopenjdk.betterrev.ArquillianTransactionalTest;
import org.adoptopenjdk.betterrev.models.Tag;
import org.junit.Before;
import org.junit.Test;

/**
 * Basic Tag persistence tests.
 * 
 * TODO could insert data from "initial-data.yml"
 */
public class TagPersistenceTest extends ArquillianTransactionalTest {

    private Tag testTag;
    
    private static final String TEST_TAG_NAME = "Test Tag";
    private static final String TEST_TAG_DESCRIPTION = "Test Tag Description";

    // TODO Parameterise the ArquillianTransactionalTest class so we can simply 
    // pass in what data to setup and destroy in each case
    @Before
    @Override
    public void preparePersistenceTest() throws Exception {
        clearData();
        insertData();
        startTransaction();
    }

    private void clearData() throws Exception {
        userTransaction.begin();
        entityManager.joinTransaction();
        entityManager.createQuery("DELETE FROM Tag").executeUpdate();
        userTransaction.commit();
    }

    private void insertData() throws Exception {
        userTransaction.begin();
        entityManager.joinTransaction();
        userTransaction.commit();
        // clear the persistence context (first-level cache)
        entityManager.clear();
    }

    @Test
    public void tagHasNoIdWhenFirstCreated() throws Exception {
        testTag = new Tag(TEST_TAG_NAME, TEST_TAG_DESCRIPTION);
        assertNull(testTag.getId());
    }

    @Test
    public void tagHasIdOnSave() throws Exception {
        testTag = new Tag(TEST_TAG_NAME, TEST_TAG_DESCRIPTION);
        entityManager.persist(testTag);
        assertNotNull(testTag.getId());
    }
    
    @Test
    public void tagHasNameOnSave() throws Exception {
        testTag = new Tag(TEST_TAG_NAME, TEST_TAG_DESCRIPTION);
        entityManager.persist(testTag);
        assertEquals(TEST_TAG_NAME, testTag.getName());
    }

    @Test
    public void tagHasDescriptionOnSave() throws Exception {
        testTag = new Tag(TEST_TAG_NAME, TEST_TAG_DESCRIPTION);
        entityManager.persist(testTag);
        assertEquals(TEST_TAG_DESCRIPTION, testTag.getDescription());
    }

    @Test
    public void onlyOneTagIsPersisted() {
        testTag = new Tag(TEST_TAG_NAME, TEST_TAG_DESCRIPTION);
        entityManager.persist(testTag);
        List<Tag> tags = entityManager.createNamedQuery("Tag.findAll", Tag.class).getResultList();
        assertEquals(1, tags.size());
    }

    @Test
    public void findById() {
        testTag = new Tag(TEST_TAG_NAME, TEST_TAG_DESCRIPTION);
        entityManager.persist(testTag);
        long tagId = testTag.getId();
        Tag tag = entityManager.find(Tag.class, tagId);
        assertEquals(tag.getId(), testTag.getId());
    }

}
