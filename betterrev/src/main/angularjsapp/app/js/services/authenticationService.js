betterrevApp.factory('authenticationService', ['$window', '$http', 'environment', function ($window, $http, environment) {

    var authenticationUrl;

    $http({
        url: environment.baseURL + 'betterrev/authentication/signin',
        method: 'GET'
    }).success(function (data) {
        $window.localStorage.setItem('oauthSecret', data['oauthSecret']);
        authenticationUrl = data['authenticationUrl'];
    });


    return {
        authenticate: function () {
            $window.open(authenticationUrl, 'BitBucket SignIn', 'location=0,status=0,width=400,height=400');
        },
        isAuthenticated: function () {
            return $window.localStorage.hasOwnProperty('oauthAccessToken');
        }
    };
}]);