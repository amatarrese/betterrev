betterrevApp.config(['$translateProvider', function ($translateProvider) {

        $translateProvider.determinePreferredLanguage();
        $translateProvider.fallbackLanguage('en');
        $translateProvider.registerAvailableLanguageKeys(['en', 'gu']);
        $translateProvider
                .translations(
                        'en',
                        {
                            "contributions.list.repository.tooltip": "The (BB clone of) OpenJDK repository",
                            "contributions.list.bb.tooltip": "The BitBucket PullRequest Id",
                            'home.welcome': 'Welcome to Betterrev',
                            "home.briefDescription": "An attempt to make webrevs more human friendly.",
                            "home.newHere.altMessage": "Where Do I Start?",
                            "home.newHere": "I'm New here, Where Do I Go?",
                            "home.workflow": "Betterrev Workflow",
                            "home.list.contributionDescription": "You submit a Pull Request for your patch, Betterrev will create a Contribution",
                            "home.list.findSponsor": "You authorise Betterrev to find you a sponsor",
                            "home.list.acceptContribution": "Once OpenJDK accepts your Contribution, Betterrev will close it automatically",
                            "home.showContributions": "Show me the Contributions",
                            "home.showListContributions": "See a list of Contributions"
                        });
        $translateProvider
                .translations(
                        'gu',
                        {
                            "contributions.list.repository.tooltip": "OpenJDK રિપોઝીટરી (ની બીબી ક્લોન)",
                            "contributions.list.bb.tooltip": "પાંચ Bitbucket પુલ વિનંતી ID ને",
                            "home.welcome": "Betterrev પર આપનું સ્વાગત છે",
                            "home.briefDescription": "મૈત્રીપૂર્ણ webrevs વધુ માનવ બનાવવા માટે પ્રયાસ.",
                            "home.newHere.altMessage": "ું ક્યાંથી શરૂ કરો છો?",
                            "home.newHere": "હું જ્યાં જાઓ છો હું અહીં નવી છું?",
                            "home.workflow": "Betterrev વર્કફ્લો",
                            "home.list.contributionDescription": "તમે તમારા પેચ માટે પુલ વિનંતિ સબમિટ, Betterrev એક ફાળો બનાવશે",
                            "home.list.findSponsor": "શું તમે ખરેખર પ્રાયોજક શોધવા માટે Betterrev અધિકૃત",
                            "home.list.acceptContribution": "OpenJDK છો એનાથી સ્વીકારે જાય, Betterrev આપોઆપ બંધ કરશે",
                            "home.showContributions": "મને યોગદાન બતાવો",
                            "home.showListContributions": "યોગદાન યાદી જુઓ"
                        });
    }]);
;
