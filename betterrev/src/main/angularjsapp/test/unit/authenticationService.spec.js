describe('Authentication service', function () {

    var $httpBackend, mockLocalStorage = {}, environment, $window, authenticationService;

    beforeEach(module('betterrev'));

    beforeEach(module(function ($provide) {

        $provide.service('$window', function () {

            var self = this;
            self.newWindowOpened = false;
            self.localStorage = {
                setItem: function (key, value) {
                    mockLocalStorage[key] = value;
                }
            };
            self.open = function (url) {
                self.newWindowOpened = true;
                self.newWindowUrl = url;
            };
        });
    }));

    beforeEach(inject(function (_$httpBackend_,_environment_, _$window_, _authenticationService_) {
        $httpBackend = _$httpBackend_;
        environment = _environment_;

        $httpBackend.whenGET(environment.baseURL + 'betterrev/authentication/signin').respond(200, {
            'authenticationUrl': 'http://fake.url',
            'oauthSecret': 'secret'
        });

        $window = _$window_;
        authenticationService = _authenticationService_;
    }));

    it('should make a request to the signin url on creation', function () {
        $httpBackend.expectGET(environment.baseURL + 'betterrev/authentication/signin');
        $httpBackend.flush();
    });


    it('should open a new window with the received authentication url', function () {
        $httpBackend.flush();
        authenticationService.authenticate();

        chai.expect($window.newWindowOpened).to.be.true;
        chai.expect($window.newWindowUrl).to.equal('http://fake.url');
    });

    it('should save the received secret in the localStorage', function () {
        $httpBackend.flush();

        chai.expect(mockLocalStorage['oauthSecret']).to.be.defined;
        chai.expect(mockLocalStorage['oauthSecret']).to.equal('secret');
    });
});