package org.adoptopenjdk.betterrev.models;

import java.time.LocalDateTime;

import org.adoptopenjdk.betterrev.events.BetterrevEvent;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * TODO - This really needs to be an event or a model but not both
 * 
 * ContributionEvent that represents a lifecycle event associated with a specific
 * Contribution.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "ContributionEvent.findContributionEventById",
            query = "SELECT ce FROM ContributionEvent ce WHERE ce.id = :contributionEventId")
})
public class ContributionEvent extends BetterrevEvent {

    @GeneratedValue(strategy = IDENTITY)
    @Id
    private Long id;

    @ManyToOne
    private Contribution contribution;

    @Enumerated(EnumType.STRING)
    private ContributionEventType contributionEventType;

    private String linkToExternalInfo;

    private LocalDateTime createdDate;

    // JPA forces public no args constructor
    public ContributionEvent() {
        this.createdDate = LocalDateTime.now();
    }

    public ContributionEvent(ContributionEventType contributionEventType) {
        this();
        this.contributionEventType = contributionEventType;
    }

    public ContributionEvent(ContributionEventType contributionEventType, Contribution contribution) {
        this(contributionEventType);
        this.contribution = contribution;
    }

    public ContributionEvent(ContributionEventType contributionEventType, String linkToExternalInfo) {
        this(contributionEventType);
        this.linkToExternalInfo = linkToExternalInfo;
    }

    @Override
    public String toString() {
        return "ContributionEvent [id=" + id + ", contribution=" + getContribution() + ", contributionEventType="
                + getContributionEventType() + ", linkToExternalInfo=" + linkToExternalInfo + ", createdDate=" + createdDate
                + "]";
    }

    public Long getId() {
        return id;
    }

    // TODO Remove later
    public void setKey(Long key) {
        id = key;
    }

    public ContributionEventType getContributionEventType() {
        return contributionEventType;
    }

    public void setContributionEventType(ContributionEventType contributionEventType) {
        this.contributionEventType = contributionEventType;
    }

    public Contribution getContribution() {
        return contribution;
    }

    public void setContribution(Contribution contribution) {
        this.contribution = contribution;
    }
}
