package org.adoptopenjdk.betterrev.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 * Mentor entity class representing a conceptual thing that can sponsor a
 * Contribution, for example, a person, project or mailing list.
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Mentor.findMentorById",
            query = "SELECT m FROM Mentor m WHERE m.id = :mentorId")
})

public class Mentor implements Serializable {

    public enum MentorType {
        INDIVIDUAL, PROJECT, LIST
    }

    @GeneratedValue(strategy = IDENTITY)
    @Id
    private Long id;

    @NotNull
    private String name;

    // TODO @Email
    @NotNull
    private String email;

    @NotNull
    @Enumerated(EnumType.STRING)
    private MentorType mentorType;

    // TODO -
    // https://bitbucket.org/adoptopenjdk/betterrev/issue/5/decide-if-we-should-refine-mentorinterests
    @ManyToMany(cascade = CascadeType.ALL)
    public Set<Interest> interests = new HashSet<>();

    private LocalDateTime createdDate;

    // TODO See if we can do this in the JSON conversion layer instead
    @NotNull
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createdOnForDisplay;

    public Mentor() {
    }

    public Mentor(String name, String email, MentorType mentorType) {
        this.name = name;
        this.email = email;
        this.mentorType = mentorType;
        this.createdDate = LocalDateTime.now();
        this.createdOnForDisplay = Date.from(createdDate.toInstant(ZoneOffset.UTC));
    }

    public static List<Mentor> findRelevantMentors(String repository, Set<String> paths) {
        List<Mentor> relevantMentors = new ArrayList<>();
        // TODO find the JavaEE way
        // List<Mentor> allMentors = find.all();
        List<Mentor> allMentors = new ArrayList<>();

        allMentors.stream().filter((mentor) -> (mentor.caresAbout(repository, paths)))
                .forEach((mentor) -> {
                    relevantMentors.add(mentor);
                });
        return relevantMentors;
    }

    private boolean caresAbout(String repository, Set<String> paths) {
        return interests.stream().anyMatch((interest) -> (interest.caresAbout(repository, paths)));
    }

    @Override
    public String toString() {
        return "Mentor [id=" + id + ", name=" + name + ", email=" + getEmail() + ", mentorType=" + mentorType
                + ", interests=" + interests + ", createdDate=" + createdDate + "]";
    }

    public Long getId() {
        return id;
    }

    // TODO remove later
    public void setKey(Long key) {
        id = key;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
