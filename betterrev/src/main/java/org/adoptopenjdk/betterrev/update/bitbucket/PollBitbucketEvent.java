package org.adoptopenjdk.betterrev.update.bitbucket;

import org.adoptopenjdk.betterrev.events.BetterrevEvent;

/**
 * @author Edward Yue Shung Wong
 */
public final class PollBitbucketEvent extends BetterrevEvent {
    // No implementation
}
